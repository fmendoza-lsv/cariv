import { DevTeam } from "./DevTeam";
import { useState, useEffect } from "react";

import URL from '../../database/dev-team-list.json'

const styleText ={
    
    fontSize: '4em',
    margin: '3em 36% 10px 36%',
    

}


function DevSearcher({ devSearched, onSearch }) {
    return (
      <div className='container'>
        <div className='d-flex justify-content-end py-4 px-2'>
          <div className='form bg-darsk p-4 px-0 '>
            <input
              type='search'
              className='form-control rounded d-inline-block border-primary'
              onChange={(evento) => onSearch(evento.target.value)}
              placeholder='Developer to search... '
              style={{ width: '360px' }}
              value={devSearched}
              autoFocus
            />
          </div>
        </div>
      </div>
    );
  }



export function DeveloperTeam() {

    const [devItem, setDevItem] = useState([]);
    const [devSearch, onSearch] = useState('');
    let person = devItem;
  
    useEffect(() => {
      setDevItem(URL.developerteamlist);
      console.log(URL);
    }, []);
  
    if (devSearch && devItem) {
      person = devItem.filter((devitem) => {
        let nameLowerCase = devitem.name.toLowerCase();
        let searchLowerCase = devSearch.toLocaleLowerCase();
        return nameLowerCase.includes(searchLowerCase);
      });
    }
  
    return (
      <div>
        <h1 style={styleText} >
            Developer Team
            </h1>
        
        <DevSearcher devSearched={devSearch} onSearch={onSearch} />
  
        <div className="container" style={{marginBottom:'50px'}}>
         <div className="row g-0" >
            {person
                ? person.map((devitem) => {
                    return <DevTeam key={devitem.id} {...devitem} />;
                })
                : 'Loading Devs...'}
            </div>
         </div>
      </div>
    );
}


