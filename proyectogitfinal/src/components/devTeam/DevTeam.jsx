const styleCards ={

    height: '310px',
    width:'550px',
    border: '1px solid gray',
    boxShadow: '2px  2px 3px 4px rgba(53, 53, 53, 0.3)',
    borderRadius: '20px',
    margin:'2.5em',
    overflow:'hidden'

}

export function DevTeam({
    name,
    photo, 
    description
    }) {

    return(
        <div className="col-6 " style={styleCards}>
            <div className="card mb-3" style={{border:'none', height:'100%', width:'100%'}}>
                <div className="row g-0"style={{height:'100%', width:'100%'}} >
                    <div className="col-md-4">
                    <img 
                    src={photo} 
                    alt={name}
                    className="img-fluid rounded-start" 
                    style={{ height: '100%', width:'100%'}}
                    />
                    </div>
                    <div className="col-md-8">
                    <div className="card-body">
                        <h5 className="card-title text-center">{name}</h5>
                        <p className="card-text">{description}</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}