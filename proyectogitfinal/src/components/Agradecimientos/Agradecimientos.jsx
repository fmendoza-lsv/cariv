import bannerImg from '../../assets/img/banner.png';
import { CardAgradecimiento } from './CardAgradecimiento.jsx';

export function Agradecimientos() {
  return (
    <div>
      <div className='card mb-3'>
        <img
          style={{ height: '150px', objectFit: 'cover' }}
          src={bannerImg}
          className='card-img-top'
          alt='banner'
        />
        <div className='card-body'>
          <h1 className='card-title text-center fs-1'>
            ¡Muchas gracias Profesor Fredy!
          </h1>
        </div>
      </div>
      <div className='container'>
        <div className='row'>
          <CardAgradecimiento
            nombre={'María Anaya Guardo'}
            descripcion={
              'Con este proyecto pudimos integrar no solo lo aprendido en GIT sino en SCRUM y REACT. valorar a quien comparte conocimiento, es un regalo maravilloso ¡ muchas gracias !'
            }
          />
          <CardAgradecimiento
            nombre={'Erick Contreras Barrios'}
            descripcion={
              'Muchas Gracias Profe fredy, fue muy duro ser PM pero aprendi demasiado, espero vernos pronto en nokia, fue muy chevere el tiempo compartido juntos.....¡ muchas gracias !'
            }
          />
          <CardAgradecimiento
            nombre={'Zuleydis Barrios Peña'}
            descripcion={
              'Profe Fredy, muchas gracias por el gran apoyo, gracias por esas clases de gitlab y me pareció super este proyecto pues con esto consolidamos lo aprendido en todas las demas clases.'
            }
          />
          <CardAgradecimiento
            nombre={'Jorge Luis Zetien Luna'}
            descripcion={
              'Profesor muchas gracias por dictarnos su conocimiento adquirido y sus experiencias en sobre el git, Aprendi bastante ya que es vital para futuros proyectos'
            }
          />
          <CardAgradecimiento
            nombre={'Leandro Meza vasquez '}
            descripcion={
              'Gracias Profe Fredi por tu paciencia y tu entrega a la hora de enseñarnos, por ponernos retos que nos hacen crecer como personas y profesionales.'
            }
          />
          <CardAgradecimiento
            nombre={'Amanda Marcela Quintana Julio'}
            descripcion={
              'Fue un excelente curso. Tuve la oportunidad de aprender muchos de los comandos de Git y ponerlos en practica en muchos proyectos. Muchas gracias!!!'
            }
          />
          <CardAgradecimiento
            nombre={'Ronald Paternina Castro'}
            descripcion={
              'Muchas gracias por compartir sus conocimientos de git y gitlab con nosotros profe (￣▽￣)//, ojalá en un futuro pueda impartirnos más cursos o compartir sus experiencias profesionales.'
            }
          />
          <CardAgradecimiento
            nombre={'Angélica Morales Daza'}
            descripcion={
              'Fue un excelente donde se aprendio acerca de los comandos y buenas prácticas en gitlab, gracias por todo el conocimiento impartido :)'
            }
          />

          {/* cambios apartir de aqui para los que falten por agradecimiento */}

          <CardAgradecimiento
            nombre={'Alan de Jesus Sanchez Tovar'}
            descripcion={
              'Muchas gracias por todo profesor, fue un excelente curso lleno de conocimientos y muchas buenas experiencias, espero nos vuelva a dar clase pronto, o nos encontremos en el backend team de Nokia. '
            }
          />
          <CardAgradecimiento nombre={'Alvaro'} descripcion={'Con este proyecto pude aplicar los conocimiento adquiridos durante el primer ciclo del car IV, ¡ muchas gracias LSV-TECH !'} />
          <CardAgradecimiento
            nombre={'Mauricio Andrés Arias Rodelo'}
            descripcion={
              'Si tuviera que dar las gracias por la paciencia que me tuviste, quizá nunca acabaría. Jamás olvidaré la dedicación y cariño con la que me enseñaste.'
            }
          />

          <CardAgradecimiento
            nombre={'Juan José Payares Trocha'}
            descripcion={
              'Profe, gracias por impartir un curso dinamico y practico, contextualizando el conocimiento y mostrandonos las utilidad mas alla del concepto. ¡Gracias!'
            }
          />
          <CardAgradecimiento
            nombre={'Luis Payares'}
            descripcion={'Fue un excelente curso, afiancé conocimientos y aprendí muchas cosas nuevas. Le agradezco su paciencia, dedicación y profesionalismo. Fue un gusto ser su alumno y espero nos encontremos pronto ya como compañeros de trabajo en proyectos futuros y ojalá ser amigos. '}
          />
          <CardAgradecimiento
            nombre={'Danith Babilonia Meza'}
            descripcion={'✨Por su amabilidad, y ganas de dar de todo ese conocimiento que ha adquirido, quiero darle las gracias, profe Fredy. Fue un placer haber tomado de su experiencia, sé que todo esto que nos ha enseñando más adelante nos será de gran ayuda, que Dios le bendiga grandemente. ✨'} />
          <CardAgradecimiento
            nombre={'Luis Tilve'}
            descripcion={'Gracias por todo lo enseñado, fue una experiencia agradable.'}
          />
          <CardAgradecimiento nombre={'Jhonatan Alvarez D'} descripcion={'Gracias por los conocimientos impartido y siga asi no cambie'} />
        </div>
      </div>
    </div>
  );
}
