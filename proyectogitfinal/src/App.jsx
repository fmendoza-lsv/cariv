import { Link, Route, Routes } from 'react-router-dom';
import { Comandos } from './components/commands/CommandList';
import { DeveloperTeam } from './components/devTeam/ListDevItem';
import { Footer } from './components/footer/Footer';
import { Navbar } from './components/navbar/Navbar';
import { LideresProyecto } from './components/LideresProyecto/LideresProyecto';


import { Home } from './Home/Home'

import { Agradecimientos } from './components/Agradecimientos/Agradecimientos';
function App() {
  return (
    <div>
      <Navbar />
      {
        // NOTA: LOS COMPONENTES DEBEN TENER EL MISMO NOMBRE QUE COLOQUE EN LAS ETIQUETAS
        // QUE ESTAN DENTRO DE LAS LLAVES

        <Routes>
          {<Route path='/' element={<Home />} />
          /*<Route path='/LideresProyecto' element={<LideresProyecto />} />
          <Route path='/Agradecimientos' element={<Agradecimientos />} />*/}
          <Route path='/Comandos' element={<Comandos />} />
          <Route path='/DeveloperTeam' element={<DeveloperTeam />} />
          <Route path='/Agradecimientos' element={<Agradecimientos />} />
          <Route path='/LideresProyecto' element={<LideresProyecto />} />
        </Routes>
      }
      <Footer />
    </div>
  );
}

export default App;
